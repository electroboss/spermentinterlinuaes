language_names = ["french", "italian", "portuguese", "spanish"]

words = ['English', 'French', 'Spanish', 'Portuguese', 'Italian', 'Russian', 'Japanese', 'Chinese', 'Korean', 'German', 'Indonesian', 'Latin', 'Norweigian', 'Swedish', 'Finnish', 'Icelandic']



from selenium import webdriver as wd
from time import sleep
driver = wd.Firefox()

translations = []

def isLoaded(driver):
  try:
    driver.find_element_by_id("focusOnMe")
    return True
  except:
    return False

driver.get("https://nicetranslator.com")
while not isLoaded(driver):
  pass

y = driver.find_element_by_id("focusOnMe")

for language_name in language_names:
  y.click()
  y.send_keys(language_name)
  driver.find_element_by_class_name("svelte-bdnybl").click()

x = driver.find_element_by_id("mainInput")
for word in words:
  x.clear()
  x.send_keys(word)
  sleep(2)
  translations.append([x.text for x in driver.find_elements_by_class_name("translation")])

print(translations)
